var app = angular.module('BarHelanoApp',[/**'ngSanitize','ngMaterial','ngRoute','ui.ace','ng-file-input','ngTable','ngAnimate','chart.js','dndLists','ngImgCrop'**/]);

app.controller('MainController', function($scope,$http/**,$http,$mdDialog,NgTableParams,ngTableEventsChannel**/) {
	
	$scope.appUrl = 'http://www.bardohelano.com.br';
	$scope.init = function () {
		$http.get($scope.appUrl + '/cms/fotos/listar').then(function(response) {
			var uri,res;
			var pastas = response.data;
			var pastaValida = [];
			for (var i = 0; i < pastas.length; i++) {
				pastas[i].linkFoto = pastas[i].FOT_Path.split("..")[1];
				if ((pastas[i].linkFoto) && (pastas[i].linkFoto.indexOf(".ftpquota") < 0) && (pastas[i].linkFoto.indexOf("main.jpg") < 0)) {				 	
				 	uri = encodeURI(pastas[i].linkFoto);
				 	pastas[i].linkFoto = uri;
				 	console.log(pastas[i].linkFoto);
				 	pastaValida.push(pastas[i]);
			 	}
			} 
			$scope.pastas = pastaValida;
			
			console.log($scope.pastas);
		});
		
	}
	
});
